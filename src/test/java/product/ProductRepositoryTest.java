package product;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//tests not ready yet
//must implement
@RunWith(MockitoJUnitRunner.class)
public class ProductRepositoryTest {

    private static final String VALID_CODE = "234";
    private static final String INCORRECT_CODE = "11111";

    @InjectMocks
    ProductRepository productRepository = new ProductRepository();
    @Mock
    List<Product> productList = new ArrayList<>();

    @Test
    public void shouldReturnValidProduct() {
        //given
        //when
        //then
    }


    private static void seedProducts() {
        Product product1 = new Product("cola", "234", new BigDecimal(2.60));
        Product product2 = new Product("mars", "245", new BigDecimal(1.75));
        ProductRepository.getInstance().addProductsToList(Arrays.asList(product1, product2));
    }
}