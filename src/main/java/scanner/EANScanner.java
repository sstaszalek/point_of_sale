package scanner;

/**
 * Model class for representing EAN bar code scanner
 * should implement real scanner
 */
public class EANScanner implements IScanner {


    @Override
    public String read(String inputCode) {

        //implementation of EAN code Scanner
        if(inputCode != null) {
            //return valid bar code type corresponding to the one in database
            return inputCode;
        } else {
            //Should implement error handling
            return "Error: Cannot read code";
        }
    }
}
