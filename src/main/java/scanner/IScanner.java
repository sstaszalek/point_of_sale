package scanner;

/**
 * IScanner interface
 * Application assumes possible implementation of different Scanners depending on bar code type
 */
public interface IScanner {

    String read(String inputCode);
}
