package product;


import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

/**
 * Model class for representing Product
 * possibility of applying real SQL DB
 * implicitly with Hibernate framework
 */

public class Product implements Serializable {

    private String barCode;
    private String name;
    private BigDecimal price;

    public Product() {
    }

    public Product(String name, String barCode, BigDecimal price) {
        this.name = name;
        this.barCode = barCode;
        this.price = price;
    }

    public String getName() {
        return name;
    }

    public String getBarCode() {
        return barCode;
    }

    public BigDecimal getPrice() {
        return price.setScale(2, BigDecimal.ROUND_CEILING);
    }

    @Override
    public String toString() {
        return String.format("position: %s price: %s", name, getPrice());
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (obj instanceof Product) {
            Product otherProduct = (Product) obj;
            return Objects.equals(barCode, otherProduct.barCode) &&
                    Objects.equals(name, otherProduct.name) &&
                    Objects.equals(price, otherProduct.price);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, barCode, price);
    }
}
