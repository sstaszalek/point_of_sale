package product;

import java.util.ArrayList;
import java.util.List;

/**
 * Product repository containing List of Products
 * and all useful methods
 */
public class ProductRepository {

    private static ProductRepository instance = new ProductRepository();

    private List<Product> products = new ArrayList<>();

    public static ProductRepository getInstance() {
        return instance;
    }

    public Product getOneByBarCode(String barCode) {
        return this.products
                .stream()
                .filter(x -> x.getBarCode().equals(barCode))
                .findFirst()
                .orElse(null);

    }

    public void addProductsToList(List<Product> products) {
        products.forEach(this::add);
    }

    private Product add(Product product) {
        this.products.add(product);
        return product;
    }
}
