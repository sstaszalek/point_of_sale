import printer.FiscalDevice;
import product.Product;
import product.ProductRepository;
import scanner.EANScanner;
import scanner.IScanner;
import screen.LCDScreen;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Simulation of simple Point of Sale
 */
public class POS {

    private static final String EXIT = "exit";
    private static final String INVALID_CODE = "Invalid bar-code!";
    private static final String PRODUCT_NOT_FOUND = "Product not found";
    private static final String TOTAL_AMOUNT = "Total amount to be paid: ";


    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);
        IScanner scanner = new EANScanner();
        //initialize shop cart list
        List<Product> productList = new ArrayList<>();
        //initialize total sum to be paid
        BigDecimal totalSum = new BigDecimal(0);

        boolean exit = false;
        do {
            String inputCode = input.nextLine();
            if (inputCode.equals(EXIT)) {
                exit = true;
            } else if (inputCode.isEmpty()) {
                LCDScreen.display(INVALID_CODE);
            } else {
                //read input code with EAN Scanner
                String barCode = scanner.read(inputCode);
                //get product from db by barcode
                //if barcode is not present Print info
                Product product = ProductRepository.getInstance().getOneByBarCode(barCode);
                if (product != null) {
                    //Print name and price of product on the screen
                    LCDScreen.display(product.toString());
                    //add to shop cart
                    productList.add(product);

                    //add price to the sum
                    totalSum = totalSum.add(product.getPrice());
                } else {
                    LCDScreen.display(PRODUCT_NOT_FOUND);
                }
            }
        } while (!exit);
        //print receipt
        FiscalDevice.printReceipt(productList, totalSum);
        //print total sum on the screen
        LCDScreen.display(TOTAL_AMOUNT + totalSum);
    }
}
