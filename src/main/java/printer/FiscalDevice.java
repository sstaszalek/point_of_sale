package printer;

import product.Product;

import java.math.BigDecimal;
import java.util.List;

/**
 * Model of fiscal receipt printer
 */
public class FiscalDevice {

    private static final String TOTAL = "total amount: ";
    private static final String PLN = " pln";
    private static final String SPACE = " ";


    public static void printReceipt (List<Product> productList, BigDecimal total) {
        //should implement real fiscal device printer
        int number = 1;
        for (Product product : productList) {
            System.out.println(number + SPACE + product.getName()
                    + SPACE + product.getPrice() + PLN);
            number += 1;
        }
            System.out.println(TOTAL + total + PLN);
    }
}
